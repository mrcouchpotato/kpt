$(function() {
    var author = $("#data-author");
    var comment = $("#data-comment");

    $("#btn_keep").click(function(ev) {
        var data = {
            author: author.val(),
            comment: comment.val()
        };

        post("keep", kptId, data).done(function(data) {
            render("keep", data.list);
            comment.val("");
        });
    });

    $("#btn_problem").click(function(ev) {
        var data = {
            author: author.val(),
            comment: comment.val()
        };

        post("problem", kptId, data).done(function(data) {
            render("problem", data.list);

            comment.val("");
        });
    });

    $("#btn_try").click(function(ev) {
        var data = {
            author: author.val(),
            comment: comment.val()
        };

        post("try", kptId, data).done(function(data) {
            render("try", data.list);

            comment.val("");
        });
    });

    get_list("keep", kptId).done(function(data) {
        render("keep", data.list);
    }).fail(function(connection, status, message) {
        alert("予期せぬエラーが発生しました。\nページをリロードしてください。");
    });

    get_list("problem", kptId).done(function(data) {
        render("problem", data.list);
    }).fail(function(connection, status, message) {
        alert("予期せぬエラーが発生しました。\nページをリロードしてください。");
    });

    get_list("try", kptId).done(function(data) {
        render("try", data.list);
    }).fail(function(connection, status, message) {
        alert("予期せぬエラーが発生しました。\nページをリロードしてください。");
    });
});

function post(type, kptId, postData) {
    return $.ajax({
        url: "/kpt/" + type + "/" + kptId,
        type: "POST",
        dataType: "json",
        data: postData
    });
}

function get_list(type, kptId) {
    return $.ajax({
        url: "/kpt/" + type + "/" + kptId,
        type: "GET",
        dataType: "json"
    });
}

function render(type, data) {
    if (data) {
        var list = $(data);

        list.find(".btn-delete").click(function(ev) {
            var button = $(ev.currentTarget);
            var post_id = button.attr("post-id");

            if (confirm("投稿を削除します。\nよろしいですか？")) {
                $.ajax({
                    url: "/kpt/remove",
                    type: "POST",
                    dataType: "json",
                    data: {post_id: post_id}
                }).done(function(data) {
                    button.closest("li.list-group-item").remove();
                });
            }

            return false;
        });

        $("." + type + " ul").empty().append(list);
    }
}
