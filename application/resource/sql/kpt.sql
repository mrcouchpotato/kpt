/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50620
 Source Host           : localhost
 Source Database       : kpt

 Target Server Type    : MySQL
 Target Server Version : 50620
 File Encoding         : utf-8

 Date: 05/25/2015 13:44:50 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `kpt`
-- ----------------------------
DROP TABLE IF EXISTS `kpt`;
CREATE TABLE `kpt` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `kpt_datetime` datetime DEFAULT NULL,
  `description` text,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `delete_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_project_id_and_kpe_datetime` (`project_id`,`kpt_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='KPT一覧';

-- ----------------------------
--  Table structure for `kpt_post`
-- ----------------------------
DROP TABLE IF EXISTS `kpt_post`;
CREATE TABLE `kpt_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kpt_id` bigint(20) unsigned NOT NULL,
  `author_id` char(22) NOT NULL,
  `author_name` varchar(20) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `comment` text,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_kpt_id_and_user_id` (`kpt_id`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投稿一覧';

-- ----------------------------
--  Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='プロジェクト一覧';

SET FOREIGN_KEY_CHECKS = 1;
