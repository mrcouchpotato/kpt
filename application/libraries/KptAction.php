<?php
require_once APPPATH . "core/BaseLibrary.php";
require_once APPPATH . "libraries/exceptions/KptException.php";
// require_once APPPATH . "libraries/qrcode/Qrcode_image.php";
require_once APPPATH . "third_party/qrcode/libraries/Qrcode_image.php";

class KptAction extends BaseLibrary
{
    const PROJECT_ID_VALIDATION_ERROR = 1001;

    const KPT_ID_VALIDATION_ERROR = 1002;

    const TITLE_VALIDATION_ERROR = 1011;

    const KPT_DATETIME_VALIDATION_ERROR = 1021;

    const DESCRIPTION_VALIDATION_ERROR = 1031;

    const AUTHOR_ID_VALIDATION_ERROR = 1041;

    const AUTHOR_NAME_VALIDATION_ERROR = 1051;

    const COMMENT_VALIDATION_ERROR = 1061;

    const POST_ID_VALIDATION_ERROR = 1071;

    const KPT_NOT_EXISTS_ERROR = 1101;

    const KPT_POST_FAILED_ERROR = 1201;

    const KPT_POST_NOT_EXISTS_ERROR = 1202;

    const KPT_POST_DELETE_DENIED_ERROR = 1203;

    const AUTHOR_NAME_MAX_LENGTH = 20;

    public function __construct()
    {
        parent::__construct();

        $this->load->library("Validation");
    }

    /**
     * KPT add
     */
    public function add($project_id, $date, $title, $description)
    {
        $result = false;

        // validation
        $this->validation->id($project_id, "project id", null, self::PROJECT_ID_VALIDATION_ERROR);

        $this->validation->required($title, "\"date\" is required.", self::KPT_DATETIME_VALIDATION_ERROR);
        $this->validation->check(
            preg_match("/^[1,2][0-9]{3}-[0-1][0-9]-[0-3][0-9]|[1,2][0-9]{3}\/[0-1][0-9]\/[0-3][0-9]$/", $date),
            "\"date\" format is yyyy-mm-dd or yyyy/mm/dd",
            self::KPT_DATETIME_VALIDATION_ERROR
        );

        $this->validation->required($title, "\"title\" is required.", self::TITLE_VALIDATION_ERROR);

        $this->validation->required($description, "\"description\" is required.", self::DESCRIPTION_VALIDATION_ERROR);

        // TODO: project id の存在チェックをする

        $kpt = $this->model("kpt");

        if ($kpt->add($project_id, $title, $date, $description)) {
            $result = $kpt->insert_id();
        }

        return $result;
    }


    /**
     * KPT createQR
     */
    public function createQR($source, $num)
    {
      $this->load->add_package_path(APPPATH.'third_party/qrcode/');
        $this->load->library('Qrcode_image');
        $this->qrcode_image->set_qrcode_version(2);
        $this->qrcode_image->set_qrcode_error_correct("L");
        $this->qrcode_image->qrcode_image_out($source.$num, 'png','application/cache/qrcode/'.$num.'.png' );
    }    


    /**
     * KPT edit
     */
    public function edit($id)
    {
        // validation
        $this->validation->id($id, "kpt id", null, self::KPT_ID_VALIDATION_ERROR);

        $kpt_data = $this->model("kpt")->getById($id);

        if ($kpt_data) {
            $default = [
                'title' => $kpt_data['title'],
                'date' => date("Y-m-d", strtotime($kpt_data['kpt_datetime'])),
                'description' => $kpt_data['description']
            ];

            $input = $this->session->flashdata("input");

            if ($input) {
                $input = array_merge($default, $input);
            } else {
                $input = $default;
            }

            $result = [
                'title' => "ふりかえり 設定変更",
                'action_uri' => "/kpt/update",
                'project' => [
                    'id' => $kpt_data['project_id'],
                    'name' => $kpt_data['project_name']
                ],
                'input' => $input,
                'kpt_id' => $kpt_data['id'],
                'submit_label' => "Save"
            ];
        } else {
            throw new KptException(
                sprintf("kpt data not exists. (id: %s)", $id),
                self::KPT_NOT_EXISTS_ERROR
            );
        }

        return $result;
    }

    /**
     * KPT update
     */
    public function update($date, $title, $description, $kpt_id)
    {
        // validation
        $this->validation->id(
            $kpt_id,
            "kpt id",
            null,
            self::KPT_ID_VALIDATION_ERROR
        );

        $this->validation->required(
            $title,
            "\"date\" is required.",
            self::KPT_DATETIME_VALIDATION_ERROR
        );
        $this->validation->check(
            preg_match("/^[1,2][0-9]{3}-[0-1][0-9]-[0-3][0-9]|[1,2][0-9]{3}\/[0-1][0-9]\/[0-3][0-9]$/", $date),
            "\"date\" format is yyyy-mm-dd or yyyy/mm/dd",
            self::KPT_DATETIME_VALIDATION_ERROR
        );

        $this->validation->required(
            $title,
            "\"title\" is required.",
            self::TITLE_VALIDATION_ERROR
        );

        $this->validation->required(
            $description,
            "\"description\" is required.",
            self::DESCRIPTION_VALIDATION_ERROR
        );

        return $this->model("kpt")->update($date, $title, $description, $kpt_id);
    }

    /**
     * KPT show
     */
    public function show($id)
    {
        $result = [];

        // validation
        $this->validation->id($id, "kpt id", null, self::KPT_ID_VALIDATION_ERROR);

        // モデルロード
        $kpt_data = $this->model("kpt")->getById($id);

        if ($kpt_data) {
            $result = [
                'kpt_id' => $kpt_data['id'],
                'project_id' => $kpt_data['project_id'],
                'project_name' => $kpt_data['project_name'],
                'title' => $kpt_data['title'],
                'datetime' => strtotime($kpt_data['kpt_datetime']),
                'description' => $kpt_data['description'],
                'author' => strval($this->session->userdata("author"))
            ];
        } else {
            throw new KptException(
                sprintf("kpt data not exists. (id: %s)", $id),
                self::KPT_NOT_EXISTS_ERROR
            );
        }

        return $result;
    }


    /**
     * KPT print out
     */
    public function printout($id)
    {
        $result = [];

        // validation
        $this->validation->id($id, "kpt id", null, self::KPT_ID_VALIDATION_ERROR);

        // モデルロード
        $kpt_data = $this->model("kpt")->getById($id);

        if ($kpt_data) {
            $kpt_post = $this->model("kpt_post");

            $result = [
                'kpt_id' => $kpt_data['id'],
                'project_id' => $kpt_data['project_id'],
                'project_name' => $kpt_data['project_name'],
                'title' => $kpt_data['title'],
                'datetime' => strtotime($kpt_data['kpt_datetime']),
                'description' => $kpt_data['description'],
                'keep' => [],
                'problem' => [],
                'try' => []
            ];

            $post_list = $kpt_post->getByKptId($kpt_data['id']);

            foreach ($post_list as $post) {
                switch ($post['type']) {
                    case Kpt_post_model::TYPE_KEEP:
                        $result['keep'][] = $post;
                        break;
                    case Kpt_post_model::TYPE_PROBLEM:
                        $result['problem'][] = $post;
                        break;
                    case Kpt_post_model::TYPE_TRY:
                        $result['try'][] = $post;
                        break;
                }
            }
        } else {
            throw new KptException(
                sprintf("kpt data not exists. (id: %s)", $id),
                self::KPT_NOT_EXISTS_ERROR
            );
        }

        return $result;
    }

    /**
     * KPT copy
     */
    public function copy($id)
    {
        // validation
        $this->validation->id($id, "kpt id", null, self::KPT_ID_VALIDATION_ERROR);

        $kpt = $this->model("kpt");
        $kpt->select([
            "project_id",
            "title",
            "kpt_datetime as date",
            "description"
        ])->where(['id' => $id, 'delete_datetime' => null]);

        // コピー元KPTデータ取得
        $result = $kpt->get()->row_array();

        if (empty($result)) {
            throw new KptException(
                sprintf("kpt data not exists. (id: %s)", $id),
                self::KPT_NOT_EXISTS_ERROR
            );
        } else {
            $result['date'] = date("Y-m-d", strtotime($result['date']));
        }

        return $result;
    }

    /**
     * KPT get post list
     */
    public function getPostList($type, $kpt_id)
    {
        $result = [
            'count' => 0,
            'list' => null
        ];

        // validation
        $this->validation->id($kpt_id, "kpt id", null, self::KPT_ID_VALIDATION_ERROR);

        $data = [];

        // user_id取得
        $data['user_id'] = $this->session->userdata("user_id");

        // get post list
        $data['list'] = $this->model("kpt_post")->{"get" . ucfirst($type) . "List"}($kpt_id);

        if (!empty($data['list'])) {
            $result['count'] = count($data['list']);
            $result['list'] = $this->load->view("kpt/list", $data, true);
        }

        return $result;
    }

    /**
     * KPT post
     */
    public function post($type, $kpt_id)
    {
        $result = [
            'post' => null
        ];

        // validation
        $this->validation->id($kpt_id, "kpt id", null, self::KPT_ID_VALIDATION_ERROR);

        $author_id = $this->session->userdata("user_id");
        $this->validation->required(
            $author_id,
            "\"author_id\" is required.",
            self::AUTHOR_ID_VALIDATION_ERROR
        );
        $this->validation->check(
            preg_match("/^[0-9,a-z]{22}$/", $author_id),
            "\"author_id\" is invalid",
            self::AUTHOR_ID_VALIDATION_ERROR
        );

        $author_name = $this->input->post("author");
        $this->validation->required(
            $author_name,
            "\"author name\" is required.",
            self::AUTHOR_NAME_VALIDATION_ERROR
        );
        $this->validation->check(
            mb_strlen($author_name) <= self::AUTHOR_NAME_MAX_LENGTH,
            sprintf("\"author name\" length is %s", self::AUTHOR_NAME_MAX_LENGTH),
            self::AUTHOR_NAME_VALIDATION_ERROR
        );

        $comment = $this->input->post("comment");
        $this->validation->required(
            $comment,
            "\"comment\" is required.",
            self::COMMENT_VALIDATION_ERROR
        );

        // author name saved session
        $this->session->set_userdata("author", $author_name);

        $success = $this->model("kpt_post")->{"add" . ucfirst($type)}(
            $kpt_id,
            $author_id,
            $author_name,
            $comment
        );

        if ($success) {
            $result = $this->getPostList($type, $kpt_id);
        } else {
            throw new KptException(
                sprintf("%s post failed.", $type),
                self::KPT_POST_FAILED_ERROR
            );
        }

        return $result;
    }

    /**
     * KPT remove post
     */
    public function removePost()
    {
        $result = false;

        // validation
        $post_id = $this->input->post("post_id");
        $this->validation->id($post_id, "post id", null, self::POST_ID_VALIDATION_ERROR);

        $author_id = $this->session->userdata("user_id");
        $kpt_post = $this->model("kpt_post");

        // get post data
        $post = $kpt_post->getById($post_id);

        if ($post) {

            if (!empty($author_id) && $post['author_id'] == $author_id) {
                $result = $kpt_post->delete(['id' => $post_id]);
            } else {
                throw new KptException(
                    sprintf("post delete permission denied. (id: %s)", $post_id),
                    self::KPT_POST_DELETE_DENIED_ERROR
                );
            }

        } else {
            throw new KptException(
                sprintf("kpt post not exists. (id: %s)", $post_id),
                self::KPT_POST_NOT_EXISTS_ERROR
            );
        }

        return $result;
    }

    /**
     * KPT delete
     */
    public function delete($id)
    {
        return [];
    }
}
