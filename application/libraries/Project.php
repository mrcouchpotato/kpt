<?php
require_once APPPATH . "core/BaseLibrary.php";

class Project extends BaseLibrary
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library("Validation");
    }

    public function get($id)
    {
        $result = null;

        $this->validation->id($id, "project id");

        // Modelロード
        $project = $this->model("project");

        $result = $project->getById($id);

        return $result;
    }

    public function getAll()
    {
        return $this->model("project")->get()->result_array();
    }
}
