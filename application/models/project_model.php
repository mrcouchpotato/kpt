<?php
class Project_model extends MY_Model
{
    protected $table_name = "project";

    public function getById($id)
    {
        $result = null;
        $this->where('id', $id);
        $query = $this->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
        }

        return $result;
    }
}
