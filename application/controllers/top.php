<?php
class Top extends MY_Controller {
    const PAGE_KPT_AMOUNT = 10;

    /**
     * トップページ
     *
     * TODO: ざっくり実装してるため、あとでリファクタリングする
     */
    public function index($project_id = null, $page = 1)
    {
        try {
            $data = [];
            $page = (int) $page;

            if ($page <= 0) {
                $page = 1;
            }

            $this->load->library("project");

            $project_list = $this->project->getAll();

            if (empty($project_list)) {
                show_error("no project");
            } else {
                foreach ($project_list as $project) {
                    if (empty($project_id) || $project['id'] == $project_id) {
                        $data['current_project_id'] = $project['id'];
                        break;
                    }
                }

                if (empty($data['current_project_id'])) {
                    throw new Exception("no match id project.");
                }

                $data['project_list'] = $project_list;

                $this->load->model("kpt_model");

                $count = $this->kpt_model->countByProjectId(
                    $data['current_project_id']
                );

                if (self::PAGE_KPT_AMOUNT < $count) {
                    $data['pager'] = true;
                    $data['prev'] = $page - 1;
                    $max_page = ceil($count / self::PAGE_KPT_AMOUNT);
                    $data['next'] = ($max_page > $page) ? $page + 1 : 0;
                } else {
                    $data['pager'] = false;
                }

                $data['kpt_list'] = $this->kpt_model->getByProjectId(
                    $data['current_project_id'],
                    $page,
                    self::PAGE_KPT_AMOUNT
                );
            }

            $this->load->view("top/index", $data);
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    public function getqrcode($qrcode_id)
    {
      header('Content-type: image/png');
      $path = APPPATH."cache/qrcode/";
      readfile($path.$qrcode_id.'.png');
    }

}
