<?php
class Kpt extends MY_Controller {

    public function index($id = null)
    {
        $this->show($id);
    }

    public function show($id = null)
    {
        $this->load->library("KptAction", null, "action");

        try {
            $data = $this->action->show($id);

            $this->load->set_template_vars(['sub_title' => $data['title']]);
            $this->load->view("kpt/index", $data);
        } catch (Exception $ex) {
            redirect("/top");
        }

    }

    public function create($project_id)
    {
        try {
            $this->load->library("Project");

            $this->load->set_template_vars([
                'sub_title' => "新規作成",
                'css' => ["/css/kpt/input.css"]
            ]);

            $input = $this->session->flashdata("input");

            if (empty($input)) {
                $input = [];
            }

            $data = [
                'title' => "ふりかえり 新規作成",
                'action_uri' => "/kpt/add",
                'project' => $this->project->get($project_id),
                'input' => $input,
                'submit_label' => "Create"
            ];

            $this->load->view("kpt/input", $data);
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    public function add()
    {
        try {
            $result = false;

            $this->load->library("KptAction", null, "action");

            $input = [
                'project_id' => $this->input->post("project_id"),
                'date' => $this->input->post("date"),
                'title' => $this->input->post("title"),
                'description' => $this->input->post("description")
            ];

            $result = $this->action->add(
                $input['project_id'],
                $input['date'],
                $input['title'],
                $input['description']
            );

            if ($result) {
                $this->action->createQR($_SERVER["HTTP_HOST"] .'/kpt/', $result);
                redirect(sprintf("/kpt/%s", $result));
            } else {
                redirect("/top");
            }
        } catch (ValidationErrorException $ex) {
            $this->session->set_flashdata("input", $input);
            redirect(sprintf("/kpt/new/%s", $input['project_id']));
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    public function edit($id = null)
    {
        $this->load->set_template_vars([
            'sub_title' => "変更",
            'css' => ["/css/kpt/input.css"]
        ]);

        $this->load->library("KptAction", null, "action");

        try {
            $result = $this->action->edit($id);

            $this->load->view("kpt/input", $result);
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    public function update()
    {
        try {
            $result = false;

            $this->load->library("KptAction", null, "action");

            $kpt_id = $this->input->post("id");

            $input = [
                'date' => $this->input->post("date"),
                'title' => $this->input->post("title"),
                'description' => $this->input->post("description")
            ];

            $result = $this->action->update(
                $input['date'],
                $input['title'],
                $input['description'],
                $kpt_id
            );

            if ($result) {
                redirect(sprintf("/kpt/%s", $kpt_id));
            } else {
                redirect("/top");
            }
        } catch (ValidationErrorException $ex) {
            $this->session->set_flashdata("input", $input);
            redirect(sprintf("/kpt/edit/%s", $kpt_id));
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    public function copy($id = null)
    {
        try {
            $result = false;

            $this->load->library("KptAction", null, "action");

            $data = $this->action->copy($id);

            $this->session->set_flashdata("input", $data);
            redirect(sprintf("/kpt/new/%s", $data['project_id']));
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    public function keep_data($kpt_id = null)
    {
        $this->post_data("keep", $kpt_id);
    }

    public function problem_data($kpt_id = null)
    {
        $this->post_data("problem", $kpt_id);
    }

    public function try_data($kpt_id = null)
    {
        $this->post_data("try", $kpt_id);
    }

    public function remove()
    {
        $result = ['status' => 0];

        $this->load->disable_template();
        $this->output->set_content_type('application/json');

        try {
            $this->load->library("KptAction", null, "action");

            $this->action->removePost();

            $this->output->set_output(json_encode($result));
        } catch (Exception $ex) {
            $result['status'] = 1;
            $result['error'] = $ex->getMessage();

            $this->output->set_status_header(400);
            $this->output->set_output(json_encode($result));
        }
    }

    public function printout($kpt_id = null)
    {
        $this->load->library("KptAction", null, "action");

        try {
            $data = $this->action->printout($kpt_id);

            $this->load->set_template_vars(['sub_title' => sprintf("印刷: %s", $data['title'])]);

            $this->load->view("kpt/print", $data);
        } catch (Exception $ex) {
            redirect("/top");
        }
    }

    private function post_data($type, $kpt_id)
    {
        $req_method = isset($_SERVER['REQUEST_METHOD']) ? strtolower($_SERVER['REQUEST_METHOD']) : "other";
        $result = ['status' => 0];

        $this->load->disable_template();
        $this->output->set_content_type('application/json');

        try {
            $data = null;
            $this->load->library("KptAction", null, "action");

            switch ($req_method) {
                case "post":
                    $data = $this->action->post($type, $kpt_id);
                    break;

                case "get":
                    $data = $this->action->getPostList($type, $kpt_id);
                    break;

                default:
                    throw new Exception("request method is invalid.");
                    break;
            }

            // $this->output->set_content_type('text/html');
            $result = array_merge($result, $data);
            $this->output->set_output(json_encode($result));
        } catch (Exception $ex) {
            $result['status'] = 1;
            $result['error'] = $ex->getMessage();

            $this->output->set_status_header(400);
            $this->output->set_output(json_encode($result));
        }
    }

    public function getqrcode($qrcode_id)
    {
      header('Content-type: image/png');
      $path = APPPATH."cache/qrcode/";
      readfile($path.$qrcode_id.'.png');
    }

}
