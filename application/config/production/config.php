<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . "/config/config.php";

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
$config['base_url']	= 'http://kpt01.sumzap.co.jp';
