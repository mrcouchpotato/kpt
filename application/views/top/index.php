<section>
    <ul class="nav nav-tabs">
        <?php /*
        <li role="presentation" class="active"><a href="#">Massive</a></li>
        <li role="presentation" class=""><a href="/top/index/2">Rush</a></li>
        */ ?>
        <?php foreach ($project_list as $project) { ?>
        <li role="presentation"<?php if ($project['id'] == $current_project_id) { ?> class="active"<?php } ?>><a href="/top/index/<?php echo $project['id']; ?>"><?php echo $project['name']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="control">
        <a class="btn btn-default" href="/kpt/new/<?php echo $current_project_id; ?>" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    </div>

    <div class="contents">
        <?php if ($pager) { ?>
        <ul class="pager">
            <?php if ($prev) { ?>
            <li class="previous"><a href="/top/index<?php printf("/%s/%s", $current_project_id, $prev); ?>">Prev</a></li>
            <?php } ?>
            <?php if ($next) { ?>
            <li class="next"><a href="/top/index<?php printf("/%s/%s", $current_project_id, $next); ?>">Next</a></li>
            <?php } ?>
        </ul>
        <?php } ?>

        <?php if (empty($kpt_list)) { ?>
        <div class="kpt-list empty">ふりかえりがありません。</div>
        <?php } else { ?>
        <table class="kpt-list table table-striped">
            <thead>
                <tr>
                    <th>実施日</th>
                    <th>タイトル</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <?php /* <th></th> */ ?>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($kpt_list as $kpt) { ?>
                <tr>
                    <td class="date"><?php echo date("Y/m/d", strtotime($kpt['kpt_datetime'])); ?></td>
                    <td class="title"><a href="/kpt/<?php echo $kpt['id']; ?>"><?php print_default($kpt['title']); ?></a></td>
                    <td class="qrcode">
                    <a class="button" data-toggle="modal" data-target="#modal-qrcode<?php echo $kpt['id']; ?>"><span class="glyphicon glyphicon-qrcode"></span></a>
                    </td> 
                    <td class="edit">
                        <a class="button" href="/kpt/edit/<?php echo $kpt['id']; ?>"><span class="glyphicon glyphicon-edit"></span></a>
                    </td>
                    <td class="copy">
                        <a class="button" href="/kpt/copy/<?php echo $kpt['id']; ?>"><span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span></a>
                    </td>
                    <?php /*
                    <td class="delete">
                        <a class="button" href="/kpt/copy/1"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                    </td>
                    */ ?>
                </tr>
              <?php include 'modal.php'; ?>
            <?php } ?>

            </tbody>
        </table>
        <?php } ?>
    </div>
</section>
