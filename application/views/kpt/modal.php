<div class="modal" id="modal-qrcode" tabindex="-1">
     <div class="modal-dialog modal-sm">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">
                     <span aria-hidden="true">&times;</span>
                 </button>
                 <h4 class="modal-title" id="modal-label">QRコード</h4>
             </div>
             <div class="modal-body">
                <img src="/kpt/getqrcode/<?php echo $kpt_id; ?>"> 
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
             </div>
         </div>
     </div>
 </div>
