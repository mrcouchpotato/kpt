<section>
    <header>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <div class="control">
                    <?php if (isset($kpt_id)) { ?>
                    <a class="btn btn-default list" title="KPT" href="/kpt/<?php echo $kpt_id; ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>
                    <?php } ?>
                    <a class="btn btn-default list" title="一覧" href="/top/index/<?php echo $project['id']; ?>"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
                </div>
                <div class="project">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="name"><?php echo $project['name']; ?></span>
                </div>
                <?php if (!empty($title)) { ?>
                <h1><?php echo $title; ?></h1>
                <?php } ?>
                <p>ふりかえりの情報を入力してください。項目は全て入力必須です。</p>
            </div>
        </div>
    </header>
    <form class="form-horizontal" action="<?php echo $action_uri ?>" method="post">
        <div class="form-group">
            <label class="control-label col-lg-1" for="date">実施日</label>
            <div class="col-lg-2">
                <input id="date" class="form-control" type="date" name="date" value="<?php print_default_array($input, 'date'); ?>" required />
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="control-label col-lg-1">タイトル</label>
            <div class="col-lg-5">
                <input type="text" class="form-control title" id="title" name="title" value="<?php print_default_array($input, 'title'); ?>" required />
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-lg-1 control-label">概要</label>
            <div class="col-lg-7">
                <textarea class="form-control description" id="description" name="description" rows="5" required><?php print_default_array($input, 'description'); ?></textarea>
            </div>
        </div>

        <?php if (isset($kpt_id)) { ?>
        <input type="hidden" name="id" value="<?php echo $kpt_id; ?>" />
        <?php } ?>
        <input type="hidden" name="project_id" value="<?php echo $project['id']; ?>" />

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
                <button type="submit" class="btn btn-primary"><?php echo $submit_label; ?></button>
            </div>
        </div>
    </form>
</section>
