<script>
    var kptId = "<?php echo $kpt_id; ?>";
</script>
<section>
    <header>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <div class="control">
                    <a class="btn btn-default qrcode" title="QRコード" data-toggle="modal" data-target="#modal-qrcode"><span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span></a> 
                    <a class="btn btn-default edit" title="編集" href="/kpt/edit/<?php echo $kpt_id; ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                    <?php /* <a class="btn btn-default delete" title="削除" href=""><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> */ ?>
                    <a class="btn btn-default list" title="印刷" href="/kpt/print/<?php echo $kpt_id; ?>" target="printout"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
                    <a class="btn btn-default list" title="一覧" href="/top/index/<?php echo $project_id; ?>"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
                </div>
                <div class="date"><span class="glyphicon glyphicon-calendar"></span> <?php echo date("Y/m/d", $datetime); ?></div>
                <div class="project"><span class="glyphicon glyphicon-tasks"></span> <?php echo htmlspecialchars($project_name); ?></div>
                <h1><?php echo htmlspecialchars($title); ?></h1>
                <p class="description"><?php echo nl2br(htmlspecialchars($description)); ?></p>
            </div>
        </div>
    </header>
    <form class="form-horizontal" action="#" method="post">
        <div class="form-group">
            <label for="data-author" class="control-label col-lg-1">投稿者</label>
            <div class="col-lg-3">
                <input class="form-control" type="text" name="author" id="data-author" maxlength="<?php echo KptAction::AUTHOR_NAME_MAX_LENGTH; ?>" value="<?php echo htmlspecialchars($author, ENT_QUOTES | ENT_HTML5); ?>" />
            </div>
        </div>

        <div class="form-group">
            <label for="data-comment" class="col-lg-1 control-label">内容</label>
            <div class="col-lg-10">
                <textarea class="comment form-control" id="data-comment" name="comment" rows="10"></textarea>
            </div>
        </div>

        <input id="data-kpt_id" type="hidden" name="kpt_id" value="<?php echo $kpt_id; ?>">

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10">
                <button id="btn_keep" type="button" class="btn btn-success">Keep</button>
                <button id="btn_problem" type="button" class="btn btn-danger">Problem</button>
                <button id="btn_try" type="button" class="btn btn-primary">Try</button>
            </div>
        </div>
    </form>
    <div class="contents row">
        <div class="col-lg-4">
            <div class="keep panel panel-success">
                <div class="panel-heading">
                    <h2 class="panel-title">Keep</h2>
                </div>
                <ul class="list-group">
                    <li class="list-group-item default"></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="problem panel panel-danger">
                <div class="panel-heading">
                    <h2 class="panel-title">Problem</h2>
                </div>
                <ul class="list-group">
                    <li class="list-group-item default"></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="try panel panel-primary">
                <div class="panel-heading">
                    <h2 class="panel-title">Try</h2>
                </div>
                <ul class="list-group">
                    <li class="list-group-item default"></li>
                </ul>
            </div>
        </div>
    </div>
    <?php include 'modal.php'; ?>
</section>

