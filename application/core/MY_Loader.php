<?php
class MY_Loader extends CI_Loader
{
    protected $template = null;

    public function template($name, $vars = [], $return = false)
    {
        if (isset($name) && is_string($name) && "" !== $name) {

            if (empty($this->template) || $this->template['name'] != $name) {
                $ci = & get_instance();

                $this->template = [
                    'name' => $name,
                    'vars' => ['contents' => $ci->output->get_output()],
                    'disable' => false
                ];
            }

            if (!empty($vars) && is_array($vars)) {
                $this->template['vars'] = array_merge($this->template['vars'], $vars);
            }

            if ($return) {
                return parent::view($this->template['name'], $this->template['vars'], true);
            }
        }
    }

    public function view($view, $vars = [], $return = false)
    {
        if (isset($this->template) && !$this->template['disable']) {
            $contents = parent::view($view, $vars, true);

            if (empty($this->template['vars']) || !is_array($this->template['vars'])) {
                $this->template['vars'] = [
                    'contents' => $contents
                ];
            } else if (empty($this->template['vars']['contents'])) {
                $this->template['vars']['contents'] = $contents;
            }

            return parent::view($this->template['name'], $this->template['vars'], $return);
        } else {
            return parent::view($view, $vars, $return);
        }
    }

    public function get_template_vars()
    {
        return isset($this->template['vars']) ? $this->template['vars'] : [];
    }

    public function set_template_vars($vars)
    {
        if (isset($this->template['vars'])) {
            $this->template['vars'] = array_merge_recursive($this->template['vars'], $vars);
        }
    }

    public function disable_template()
    {
        if (isset($this->template['disable'])) {
            $this->template['disable'] = true;
        }
    }

    public function reset_template()
    {
        $this->template = null;
    }
}
