<?php
class BaseLibrary
{
    protected $ci = null;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public function __get($name)
    {
        return $this->ci->$name;
    }

    /**
     * モデルロード
     */
    public function model($name)
    {
        $name = strtolower($name) . "_model";

        if (!isset($this->ci->$name)) {
            $this->ci->load->model($name);
        }

        return $this->ci->$name;
    }
}
